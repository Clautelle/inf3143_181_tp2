/*
 * Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package farstar;

/**
 * A Shield can absorb all the damaged fired from a weapon.
 *
 * It need to cool down before being used again.
 */
public class Shield extends DefensiveUpgrade {

    /**
     * How many turns does this shield need to cool down before two uses?
     */
    Integer coolDownTime;

    /**
     * How many turns are left to cool down.
     *
     * When this value reaches 0, the shield can be used.
     */
    Integer coolDown = 0;

    public Shield(Integer coolDownTime) {
        super(10000 / coolDownTime, 100);
        this.coolDownTime = coolDownTime;
    }

    @Override
    public Boolean canUse(Ship usinShip) {
        return super.canUse(usinShip) && coolDown == 0;
    }

    @Override
    public Integer absorb(Integer damages) {
        coolDown = coolDownTime;
        return damages;
    }

    @Override
    public void reload(Ship ship) {
        if (coolDown > 0) {
            coolDown--;
        }
    }

    public Integer getCoolDown() {
        return coolDown;
    }
    
    @Override
    public String toString() {
        return "shield (cd: " + coolDown + "/" + coolDownTime + ")";
    }

}
