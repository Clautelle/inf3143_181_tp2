/*
 * Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package farstar;

import java.util.ArrayList;
import java.util.List;

/**
 * A round is a turn phase where a player attack another one.
 */
public class Round {

    /**
     * The player who makes the attack.
     */
    Player attackee;

    /**
     * The player targeted by the attack.
     */
    Player target;

    /**
     * Offensive upgrades to be used by the attackee to damage the target.
     */
    List<OffensiveUpgrade> attacks = new ArrayList<>();

    /**
     * Defensive upgrades to be used by the target to protect himself.
     */
    List<DefensiveUpgrade> defenses = new ArrayList<>();

    public Round(Player attackee, Player target) {
        this.attackee = attackee;
        this.target = target;
    }

    /**
     * Use an offensive upgrade from the attackee to damage the target.
     *
     * @param attack the upgrade to be used.
     */
    public void useAttack(OffensiveUpgrade attack) {
        attacks.add(attack);
    }
    
    /**
	 * Use a defensive upgrade from the target to defend himself from the
	 * attack.
	 *
	 * @param defense the upgrade to be used.
	 */
	public void useDefense(DefensiveUpgrade defense) {
	    defenses.add(defense);
	}

    /**
     * Play the found.
     *
     * This methods will use all the offensive upgrades chosen by the attackee
     * to damage the target. For each offensive upgrade used, the target will
     * use its defensive upgrades to protect itself.
     */
    public void playRound() {
        System.out.println(attackee + " attacks " + target);
        for (OffensiveUpgrade attack : attacks) {
            if (!attack.canUse(attackee.ship)) {
                continue;
            }
            Integer damages = attack.fire();
            attackee.ship.consumeEnergy(attack.getEnergyCost());
            System.out.println(attackee + " fires " + attack);
            
            for (DefensiveUpgrade defense : defenses) {
                if (!defense.canUse(target.ship)) {
                    continue;
                }
              
                Integer abs = defense.absorb(damages);
                target.ship.consumeEnergy(attack.getEnergyCost());
                System.out.println(target + " uses " + defense + " and absorbs " + abs + " damages");
                damages -= abs;
            }
           target.ship.damageHull(damages);
            if (target.ship.isDestroyed()) {
               break;
           }
        }
    }
}
