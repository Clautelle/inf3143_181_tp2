/* * Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.*/
package farstar;

import java.util.ArrayList;
import java.util.List;

/**
 * @author BUGJ21518502 - Jeanine Bugueu
 * @author QUEL16107105 - Leopold Quenum
 *
 */
public class Main {
	public static void main(String[] args) throws NotEnoughCreditsException, NotEnoughUpgradeSlotsException {

		// Players
		Player p1 = new Player("p1", 100);
		Player p2 = new Player("p2", 100);
		List<Player> players = new ArrayList<Player>();
		players.add(p1);
		players.add(p2);

		// TURN#1
		////////
		// Rounds
		Round r1 = new Round(p1, p2);
		Round r2 = new Round(p2, p1);
		List<Round> rounds = new ArrayList<Round>();
		rounds.add(r1);
		rounds.add(r2);

		Turn t1 = new Turn(1, players, rounds);

		// Players' ships
		Ship s1 = new Ship(100, 200, 250, 200);
		p1.setShip(s1);
		s1.setEnergy(198);
		s1.setHull(95);

		Ship s2 = new Ship(50, 250, 300, 200);
		p2.setShip(s2);
		s2.setEnergy(200);
		s2.setHull(100);

		// 1st round
		r1.useAttack(new Phaser(25));
		r1.useDefense(new Shield(5));
		r1.useAttack(new Blaster(50, 3));

		// 2nd round
		r2.useAttack(new Phaser(10));
		r2.useDefense(new Absorber(5));

		// Equip p1's ship
		s1.equip(new SolarPanel(10));
		s1.equip(new Robots(10));

		t1.playTurn();

		// TURN#2
		////////
		// New rounds
		rounds = new ArrayList<Round>();
		r1 = new Round(p1, p2);
		r2 = new Round(p2, p1);
		rounds.add(r1);
		rounds.add(r2);

		Turn t2 = new Turn(2, players, rounds);

		// New ships
		Ship sh1 = new Ship(100, 200, 250, 200);
		p1.setShip(sh1);
		sh1.setEnergy(193);
		sh1.setHull(95);

		Ship sh2 = new Ship(50, 250, 300, 200);
		p2.setShip(sh2);
		sh2.setEnergy(197);
		sh2.setHull(50);

		// New attacks/defenses
		r1.useAttack(new Phaser(25));
		r2.useAttack(new Phaser(10));
		r2.useDefense(new Absorber(5));

		// Equip new ships
		sh1.equip(new SolarPanel(10));
		sh1.equip(new Robots(10));

		t2.playTurn();

		// TURN#3
		////////
		// New rounds
		rounds = new ArrayList<Round>();
		r1 = new Round(p1, p2);
		rounds.add(r1);

		Turn t3 = new Turn(3, players, rounds);

		// New ships
		Ship shp1 = new Ship(100, 200, 250, 200);
		p1.setShip(shp1);
		shp1.setEnergy(200);
		shp1.setHull(100);

		Ship shp2 = new Ship(50, 250, 300, 200);
		p2.setShip(shp2);
		shp2.setEnergy(196);
		shp2.setHull(25);

		// New attacks/defenses
		r1.useAttack(new Phaser(25));

		t3.playTurn();
	}
}
