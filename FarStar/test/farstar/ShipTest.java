/**
 * @author BUGJ21518502 - Jeanine Bugueu
 * @author QUEL16107105 - Leopold Quenum
 *
 */

package farstar;

import static org.junit.Assert.*;
import org.junit.Test;

public class ShipTest {

	Ship sh;
	Upgrade sp, rb;
	
	@Test
	public void testShip_inputCost_getCreditCost() {
		sh = new Ship(21,0,0,0);
		
		Integer res = sh.getCreditsCost();
		Integer exp = 21;
		assertEquals(res, exp);
	}
	
	@Test
	public void testShip_inputMaxHull_getMaxHull() {
		sh = new Ship(0,23,0,0);
		
		Integer res = sh.getMaxHull();
		Integer exp = 23;
		assertEquals(res, exp);
	}
	
	@Test
	public void testShip_inputMaxEnergy_getMaxEnergy() {
		sh = new Ship(0,0,25,0);
		
		Integer res = sh.getMaxEnergy();
		Integer exp = 25;
		assertEquals(res, exp);
	}
	
	@Test
	public void testShip_inputUpgradeSlots_getUpgradeSlots() {
		sh = new Ship(0,0,0,28);
		
		Integer res = sh.getUpgradeSlots();
		Integer exp = 28;
		assertEquals(res, exp);
	}

	@Test(expected = NotEnoughUpgradeSlotsException.class)
	public void testEquip_upgradesSizeEqualToUpgrageSlots_raiseException() 
			throws NotEnoughUpgradeSlotsException {
		sh = new Ship(0,0,0,0);
		sh.equip(new Phaser(1));
	}
	
	@Test(expected = NotEnoughUpgradeSlotsException.class)
	public void testEquip_upgradesSizeGreaterThanUpgrageSlots_raiseException() 
			throws NotEnoughUpgradeSlotsException {
		sh = new Ship(0,0,0,0);

		sh.upgrades.add(new Phaser(0));
		sh.equip(new Phaser(1));
		 
	}
	
	@Test
	public void testEquip_upgradesSizeLessThanUpgrageSlots_upgradesContainsAddedUpgrade() 
			throws NotEnoughUpgradeSlotsException {
		sh = new Ship(0,0,0,5);
		Phaser ph = new Phaser(0);
		sh.equip(ph);
		
		Boolean res = sh.getUpgrades().contains(ph);
		Boolean exp = true;
		
		assertEquals(res, exp); 
	}

	@Test
	public void testIsDestroyed_hullEqualToZero_true() {
		sh = new Ship(0,1,0,0);
		sh.setHull(0);
		
		Boolean res = sh.isDestroyed();
		Boolean exp = true;
		assertEquals(res, exp);
	}
	@Test
	public void testIsDestroyed_hullLessThanZero_true() {
		sh = new Ship(0,1,0,0);
		sh.setHull(-1);
		
		Boolean res = sh.isDestroyed();
		Boolean exp = true;
		assertEquals(res, exp);
	}
	@Test
	public void testIsDestroyed_hullGreaterThanZero_false() {
		sh = new Ship(0,0,0,0);
		sh.setHull(5);
		
		Boolean res = sh.isDestroyed();
		Boolean exp = false;
		assertEquals(res, exp);
	}

	@Test
	public void testDamageHull_greaterThanCurrentHull_hullEqualToZero() {
		sh = new Ship(1,1,1,1);
		sh.setHull(5);
		sh.damageHull(10);
		
		Integer res = sh.getHull();
		Integer exp = 0;
		assertEquals(res, exp);
	}
	
	@Test
	public void testDamageHull_equalToCurrentHull_hullEqualToZero() {
		sh = new Ship(1,1,1,1);
		sh.setHull(5);
		sh.damageHull(5);
		
		Integer res = sh.getHull();
		Integer exp = 0;
		assertEquals(res, exp);
	}
	
	@Test
	public void testDamageHull_lessThanCurrentHull_decreasingHull() {
		sh = new Ship(1,1,1,1);
		sh.setHull(10);
		sh.damageHull(5);
		
		Integer res = sh.getHull();
		Integer exp = 5;
		assertEquals(res, exp);
	}

	@Test
	public void testRepairHull_finalHullGreaterThanMaxHull_hullEqualToMaxHull() {
		
		sh = new Ship(4,10,5,5);
		sh.setHull(0);
		sh.repairHull(15);
		
		Integer res = sh.getHull();
		Integer exp = sh.getMaxHull();
		assertEquals(res, exp);
	}
	
	@Test
	public void testRepairHull_finalHullEqualToMaxHull_hullEqualToMaxHull() {
		
		sh = new Ship(4,10,5,5);
		sh.setHull(0);
		sh.repairHull(10);
		
		Integer res = sh.getHull();
		Integer exp = sh.getMaxHull();
		assertEquals(res, exp);
	}
	
	
	@Test
	public void testRepairHull_finalHullLessThanMaxHull_increasingHull() {
		
		sh = new Ship(4,10,5,5);
		sh.setHull(0);
		sh.repairHull(5);
		
		Integer res = sh.getHull();
		Integer exp = 5;
		assertEquals(res, exp);
	}

	@Test
	public void testConsumeEnergy_greaterThanCurrentEnergy_energyEqualToZero() {
		sh = new Ship(0,0,0,0);
		sh.setEnergy(5);
		sh.consumeEnergy(10);
		
		Integer res = sh.getEnergy();
		Integer exp = 0;
		assertEquals(res, exp);
	}
	
	@Test
	public void testConsumeEnergy_equalToCurrentEnergy_energyEqualToZero() {
		sh = new Ship(0,0,0,0);
		sh.setEnergy(5);
		sh.consumeEnergy(5);
		
		Integer res = sh.getEnergy();
		Integer exp = 0;
		assertEquals(res, exp);
	}
	
	

	@Test
	public void testConsumeEnergy_lessThanCurrentEnergy_decreasingEnergy() {
		sh = new Ship(0,0,0,0);
		sh.setEnergy(8);
		sh.consumeEnergy(5);
		
		Integer res = sh.getEnergy();
		Integer exp = 3;
		assertEquals(res, exp);
	}
	
	@Test
	public void testAddEnergy_finalEnergyLessThanMaxEnergy_increasingEnergy() {
		sh = new Ship(0,0,10,0);
		sh.setEnergy(5);
        sh.addEnergy(3);
        
        Integer res = sh.getEnergy();
		Integer exp = 8;
		assertEquals(res, exp);
	}
	
	@Test
	public void testAddEnergy_finalEnergyEaualToMaxEnergy_energyEqualToMaxEnergy() {
		sh = new Ship(0,0,10,0);
		sh.setEnergy(7);
        sh.addEnergy(3);
        
        Integer res = sh.getEnergy();
		Integer exp = sh.getMaxEnergy();
		assertEquals(res, exp);
	}
	
	@Test
	public void testAddEnergy_finalEnergyGreaterThanMaxEnergy_energyEqualToMaxEnergy() {
		sh = new Ship(0,0,10,0);
		sh.setEnergy(4);
        sh.addEnergy(8);
        
        Integer res = sh.getEnergy();
		Integer exp = sh.getMaxEnergy();
		assertEquals(res, exp);
	}
	
	@Test
	public void testSetUpgradeSlots_upgradeSlotsEqualToGetUpgradeSlots() {
		
		sh = new Ship(0,0,0,0);
		sh.setUpgradeSlots(5);
		
		Integer res = sh.getUpgradeSlots();
		Integer exp = 5;
		assertEquals(res, exp);
	}

	@Test
	public void testSetCreditsCost_costEqualToGetCreditsCost() {
		sh = new Ship(0,0,0,0);
		sh.setCreditsCost(12);
		
		Integer res = sh.getCreditsCost();
		Integer exp = 12;
		assertEquals(res, exp);
	}

	@Test
	public void testSetHull_hullEqualToGetHull() {
		sh = new Ship(0,0,0,0);
		sh.setHull(14);
		
		Integer res = sh.getHull();
		Integer exp = 14;
		assertEquals(res, exp);
	}

	@Test
	public void testSetEnergy_energyEqualToGetEnergy() {
		sh = new Ship(0,0,0,0);
		sh.setEnergy(16);
		
		Integer res = sh.getEnergy();
		Integer exp = 16;
		assertEquals(res, exp);
	}

	@Test
	public void testSetOwner_ownerEqualToGetOwner() {
		sh = new Ship(0,0,0,0);
		Player p = new Player("p", 0);
		sh.setOwner(p);
		
		Player res = sh.getOwner();
		Player exp = p;
		assertEquals(res, exp);
	}

	@Test
	public void testReload_reloadSolarPanel_incrreaseEnergy() 
			throws NotEnoughUpgradeSlotsException  {
		sh = new Ship(0, 0, 52, 20);

		sp = new SolarPanel(10);
		sh.equip(sp);
		sh.setEnergy(5);

		sh.reload();

		Integer res = sh.getEnergy();
		Integer exp = 15;
		assertEquals(res, exp);

	}

	@Test
	public void testReload_reloadSolarPanel_cannotUse() 
			throws NotEnoughUpgradeSlotsException {
		sh = new Ship(0, 0, 52, 20);
		sh.setEnergy(-1);
		sp = new SolarPanel(10);
		sh.equip(sp);
		sh.reload();
		
		Integer res = sh.getEnergy();
		Integer exp = -1;
		assertEquals(res, exp);

	}

	@Test
	public void testReload_reloadRobots_repairHull() 
			throws NotEnoughUpgradeSlotsException  {
		sh = new Ship(0, 20, 10, 10);

		rb = new Robots(9);
		sh.equip(rb);
		sh.setHull(5);

		sh.reload();

		Integer res = sh.getHull();
		Integer exp = 14;
		assertEquals(res, exp);

	}
	
	@Test
	public void testReload_reloadRobots_cannotUse() 
			throws NotEnoughUpgradeSlotsException  {
		sh = new Ship(0, 20, 5, 10);

		rb = new Robots(9);
		sh.equip(rb);
		sh.setHull(5);

		sh.reload();

		Integer res = sh.getHull();
		Integer exp = 5;
		assertEquals(res, exp);

	}

}
