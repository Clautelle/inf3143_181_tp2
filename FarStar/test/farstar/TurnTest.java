/**
 * @author BUGJ21518502 - Jeanine Bugueu
 * @author QUEL16107105 - Leopold Quenum
 *
 */
package farstar;

import static org.junit.Assert.*;

import java.util.ArrayList;

import java.util.List;
import org.junit.Before;
import org.junit.Test;

public class TurnTest {

	Turn t;
	List<Player> players;
	Player p1, p2;
    List<Round> rounds;
    Round r;
    Ship s1, s2;
    
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		//Player list
		 players = new ArrayList<Player>();
		 p1 = new Player("p1", 100);
	     p2 = new Player("p2", 100);
	     players.add(p1);
	     players.add(p2);
	     
	    //Round list
	    rounds = new ArrayList<Round>();
	    r = new Round(p1, p2);
	    rounds.add(r);
	    
	    //Player p1's ship
	    s1 = new  Ship(50,200,250,200);
	    p1.setShip(s1);
	}
	
	@Test
	public void testPlayTurn_gameOver_true() {
	     
	     t = new Turn(1, players, rounds);
	     
	    //Player p2's ship
	     s2 = new  Ship(50,10,10,200);
	     p2.setShip(s2);
	     
	     r.useAttack(new Phaser(50));
	     
	     
	     Boolean res = t.playTurn();
	     Boolean exp = true;
	     assertEquals(res, exp);
	}
	
	@Test
	public void testPlayTurn_gameOver_false() {
	     
	     t = new Turn(2, players, rounds);
	     
	    //Player p2's ship
	     s2 = new  Ship(50,250,300,200);
	     p2.setShip(s2);
	     
	     r.useAttack(new Phaser(10));
	     
	     
	     Boolean res = t.playTurn();
	     Boolean exp = false;
	     assertEquals(res, exp);
	}

}
