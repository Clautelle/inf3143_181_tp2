/*
 * Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package contrats;

import farstar.Item;
import farstar.NotEnoughCreditsException;
import farstar.Phaser;
import farstar.Player;

public class TestPlayer06 {

    public static void main(String[] args) throws NotEnoughCreditsException {
        Player player = new Player06("p1", 100000);
        player.buy(new Phaser(10));
    }
}

class Player06 extends Player {

    public Player06(String name, Integer credits) {
        super(name, credits);
    }

    @Override
    public void buy(Item item) throws NotEnoughCreditsException {
        ownedItems.add(item);
    }
    
}
